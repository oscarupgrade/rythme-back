const express = require('express');
const authController = require('../controllers/auth.controller');
const router = express.Router();

router.post('/register', authController.userRegister);
router.post('/login', authController.userLogin);
router.post('/send-email', authController.confirmCount);
router.get('/check_session', authController.userCheck);
router.get('/logout', authController.userLogout);

module.exports = router;