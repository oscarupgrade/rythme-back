const express = require('express');
const router = express.Router();
const singersController = require('../controllers/singers.controller');

router.get('/', singersController.getSingers);
router.get('/search', singersController.getSingersPerPage);
router.get('/styles', singersController.getStyles );
router.get('/:id', singersController.getSingerById);

module.exports = router;