const mongoose = require('mongoose');
const DB_URL = require('../db.js').DB_URL;
const Events = require('../models/Events');

const events = [
    {
        date: '2021-02-21',
        time: '22:00',
        price: '55',
        tickets: 500,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-21',
        time: '21:00',
        price: '25',
        tickets: 50,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-21',
        time: '20:00',
        price: '15',
        tickets: 100,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-21',
        time: '22:00',
        price: '20',
        tickets: 30,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-21',
        time: '17:00',
        price: '25',
        tickets: 200,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-21',
        time: '12:00',
        price: '27',
        tickets: 500,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-22',
        time: '22:00',
        price: '55',
        tickets: 500,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-22',
        time: '21:00',
        price: '25',
        tickets: 50,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-22',
        time: '20:00',
        price: '15',
        tickets: 100,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-22',
        time: '22:00',
        price: '20',
        tickets: 30,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-22',
        time: '17:00',
        price: '25',
        tickets: 200,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
    {
        date: '2021-02-22',
        time: '12:00',
        price: '27',
        tickets: 500,
        assistants: 0,
        singer: '6037a5ceef006030dc3604b4',
        room: '6037a5d7f3653d45a08252a3'
    },
];

mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .then(async () =>{
        const allEvents = await Events.find();

        if (allEvents.length) {
            await Events.collection.drop();
        }
    })
    .catch((err) => {
        console.log(`Error deleting db data ${err}`);
    })
    .then(async () => {
        await Events.insertMany(events);
    })
    .catch((err) => {
        console.log(`Error adding data to our db ${err}`)
    })
    .finally(() => mongoose.disconnect());