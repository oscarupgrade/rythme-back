const passport = require("passport");
const Users = require('../models/Users');
const nodemailer = require("nodemailer");
require('dotenv').config(); 

const userRegister = (req, res, next) => {
    passport.authenticate('register',(error, user) =>{
        if (error){
            return res.status(401).json(error);
        };

        req.logIn(user, (error) =>{
            // console.log( 'auth controller user', user)
            // console.log('auth controller error', error)
            if (error){
                return res.status(401).json(error);

            } else {
                confirmCount(req, user.email)

                return  res.status(200).json({ data: req.user, message: 'User registered successful' })
            }
        });

    })(req, res, next);
};

const userLogin = (req, res, next) => {
    passport.authenticate('login', {}, (error, user) => {
        if (error) next(error);

        req.logIn(user, (error) => {
            if (error) {
                next(error);
            } else {
                return res.status(200).json({ data: req.user, message: 'Login successful' });
            }
        });

    })(req, res, next);
};

const userCheck = async (req, res) => {
  if (req.user){
      return res.status(200).json({ data: req.user, message: null });
  } else {
      return res.status(200).json({ data: null, message: 'No user found' });
  }
};

const userLogout = (req, res) => {
    if (req.user) {
        req.logout();
        req.session.destroy(() => {
            res.clearCookie("connect.sid");
            return res.status(200).json({ data: null, message: 'Logout successful' });
        });
    } else {
        return res.sendStatus(401).json({ data: null, message: 'Unexpected error' });
    }
};

const confirmCount = (req, email) =>{
    // console.log(req.headers.origin)
    const transporter = nodemailer.createTransport({
        // host: "smtp.gmail.com",
        service: 'gmail',
        // post: 1234,
        // secure: false,
        auth: {
            user: 'rythmeok@gmail.com',
            pass: `${process.env.NODEMAILER_PASSWORD}` //'upgrade@987'
        }
    });

    const mailOptions ={
        //jonay.hdez@gmail.com,
        from: "Rythme",
        to: email,
        subject: 'Email Validation',
        html: `
         <p>Thank you for registering with RYTHME. Verify your email in this enlace <a href="${req.headers.origin}/login">Verificar Cuenta</a></p>
        `
    };

    transporter.sendMail(mailOptions, (error, info) =>{
        if(error) {
           console.log('nodemailer', error) // res.status(500).send(error.message);
        } else {
            console.log('email enviado', info);
            // res.status(200).json(req.body)
        }
    })
}

module.exports = {
    userRegister,
    userLogin,
    userCheck,
    userLogout,
    confirmCount
}
